$(document).ready(function() {
  $("li").css("id", "uw");
  const states = ["idle", "gather", "process"];
  var state = states[0];
  var words = new Array();
  var ndx = 0;

  $("ul").on("mouseover", "li", function() {
    console.log("x:" + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function() {
    $(this).attr("id", "uw-gold");
  });

  $("ul").attr("id", "uw-gold");

  // reset button click
  $("button").on("click", function(e) {
    $(".list-group>li").remove();
  });

  // keypress
  $("input").on("keypress", function(e) {
    var code = e.which;
    // var char = String.fromCharCode(code);
    // console.log('key:' + code + '\tstate:' + state);
    if ($(this).val() == "") {
      state = "idle";
  }

    switch (state) {
      // idle
      case "idle":
        if ($(this).val() == "") {
          state = "gather";
        }
        break;

      // gather
      case "gather":
        
        if(code == 13){
          $(".list-group").append("<li>" + $(this).val() + "</li>");
        $(this).val('');
          state = "process";
        }
        break;
        
        

      // process
      case "process":
        
        state = "idle";
        break;

      default:
        break;
    }
  });
});
