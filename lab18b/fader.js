$(document).ready(function () {
  $('li').css('margin', '10px');
  $('li').attr('id', 'uw');

  $('#p1 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut(2000, function () {
      console.log("fadeout complete!")
    });
  });
  
  $('#p2 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut(0, function () {
      console.log("fadeout complete!")
    }).fadeIn(2000);
  });

  $('#p3 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeTo(2000, 0.1,  function () {
      console.log("fadeout complete!")
    });
  });

  $('#p4 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeToggle(2000).fadeToggle(2000);
  });
});