$(document).ready(function() {
  const base_url="https://api.weather.gov/stations/";
  const endpoint="/observations/latest";

  // weather update button click
  $('#getwx').on('click', function(e) {
    var mystation = $('input').val();
    var myurl = base_url + mystation + endpoint;
    $('input#my-url').val(myurl);
    
    // clear out any previous data
    $('ul li').each(function() {
      // enter code to clear each li
      $("ul li").empty();
    });
      
    console.log("Cleared Elements of UL");
    
    // execute AJAX call to get and render data
    $.ajax({
      url: myurl,
      dataType: "json",
      success: function(data) {
        var tempC= data['properties']['temperature'].value.toFixed(1);
        var tempF = (tempC * 9/5 + 32).toFixed(1);
        
        // get wind info and convert m/s to kts
        var windMiles = data['properties']['windSpeed'].value; 
        var windKilo = (windMiles * 1.94384).toFixed(1);

        
        // uncomment this if you want to dump full JSON to textarea
        var myJSON = JSON.stringify(data);
        $('textarea').val(myJSON);
        $('ul').empty();
        
        
       

        // add additional code here for the Wind direction, speed, weather contitions and icon image
        var str = "<li>Current Temperature: " + tempC +"C " + tempF+"F"+"</li>";
        str += "<li>Wind Speed: " + windMiles + "m/s " + windKilo + "mi/h " + "</li>";
        str += "<li>Wind Direction: " + windDirection + "°</li>";
        str += "<li>Weather Condition: <img src='https://api.weather.gov/icons/land/night/fog?size=medium' alt='weatherCondition' height=42 width=42> Fog/Mist</li>";
        $('ul').append(str);
        $('ul li').attr('class', 'list-group-item');
      }
    });
  });
});