var apikey = "9a9e3c4d4e8c44358f881784eeeac158";
var keyword = "q=tesla";

var url = "http://newsapi.org/v2/everything?" + keyword + "&apiKey=" + apikey;
console.log(url);

var req = new Request(url);
fetch(req)
    .then(function(response){
        return response.json()
    })
    .then(function(data){
        console.log(data.articles[0].urlToImage);
        console.log(data.articles[0]);
        $('#newsimage').attr("src",data.articles[0].urlToImage);
        $('#newstitle').text(data.articles[0].title);
        $('#newsmain').text(data.articles[0].description);
        $('#newsauthor').text("by: "+ data.articles[0].author);
        $('#newsmore').attr("href",data.articles[0].url);
    })