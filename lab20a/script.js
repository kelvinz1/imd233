var myurl = "https://api.coindesk.com/v1/bpi/currentprice/USD.json";
var code = "USD";
var i = 0;
var plot = [];
update();
var graph = Morris.Line({
    element: 'blah',
    data:  [{x:0,y:0}
     ],
    xkey: 'x',
    ykeys: ['y'],
    labels: ['Value'],
    hideHover: true,
    parseTime: false,

  });
function updateData(data){
    plot.push({x:i,y:data});
    graph.setData(plot)
}

function update(){
    $.ajax({
        url: myurl,
        async: false,
        success:function(data){
            var bc = JSON.parse(data);
            price = bc.bpi[code].rate;
            $('#currentprice').text(price);
            updateData(parseFloat(price.replace(/,/g, '')));
            i++;
        }
    });
    
 
    
}
setInterval(update,10000);
console.log(plot);
